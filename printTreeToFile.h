#pragma once
#include <string>
#include "BSNode.h"
#include <iostream>
#include <fstream>
#include "BSNode.cpp"

template <class data>
void printTreeToFile(const BSNode<data>* bs, std::ofstream& file);

template<class data>
void printTreeToFile(const BSNode<data>* bs, std::ofstream& file)
{
	const BSNode<data>* c = bs;
	
	file << c->getData() << " ";
	if (c->getLeft())
	{
		printTreeToFile(c->getLeft(), file);
	}
	file << "# ";
	if (c->getRight())
	{
		printTreeToFile(c->getRight(), file);
	}
}