#pragma once

#include <string>
#include <sstream>
#include <iostream>
#include <typeinfo>


template <class data>
class BSNode
{
public:

	BSNode(data data);
	BSNode(const BSNode<data>& other);

	virtual ~BSNode();

	virtual BSNode<data>* insert(data value);
	BSNode<data>* operator=(const BSNode<data>& other);

	virtual int getBalanceFactor();
	virtual BSNode<data>* rotateLeft();
	virtual BSNode<data>* rotateRight();
	bool isLeaf() const;
	data getData() const;
	BSNode<data>* getLeft() const;
	BSNode<data>* getRight() const;
	void setLeft(BSNode<data>* left);
	void setRight(BSNode<data>* right);

	bool search(data val) const;

	int getHeight() const;
	int getDepth(const BSNode<data>& root) const;

	void printNodes() const; //for question 1 part C

protected:
	data _data;
	BSNode<data>* _left;
	BSNode<data>* _right;
	
	int _count; //for question 1 part B
	int getCurrNodeDistFromInputNode(const BSNode<data>* node) const; //auxiliary function for getDepth
};

template <class data>
BSNode<data>::BSNode(data data)
{
	_data = data;
	_count = 1;
	_left = nullptr;
	_right = nullptr;
}

template <class data>
BSNode<data>::BSNode(const BSNode<data>& other)
{
	_data = other.getData();
	if (other._left)
	{
		insert(other._left->getData());
	}
	else
	{
		_left = nullptr;
	}
	if (other._right)
	{
		insert(other._right->getData());
	}
	else
	{
		_right = nullptr;
	}
	_count = other._count;
}

template <class data>
BSNode<data>::~BSNode<data>()
{
	if (_left)
	{
		delete _left;
	}
	if (_right)
	{
		delete _right;
	}
}

template <class data>
BSNode<data>* BSNode<data>::insert(data value)
{
	if (value > _data)
	{
		if (_right)
		{
			_right->insert(value);
		}
		else
		{
			_right = new BSNode(value);
			_right->_left = nullptr;
			_right->_right = nullptr;
		}
	}
	else if (value < _data)
	{
		if (_left)
		{
			_left->insert(value);
		}
		else
		{
			BSNode<data>* a = new BSNode(value);
			_left = new BSNode(value);
			_left->_left = nullptr;
			_left->_right = nullptr;
		}
	}
	else
	{
		_count++;
	}
	return this;
}

template <class data>
void BSNode<data>::printNodes() const
{
	if (_left)
	{
		_left->printNodes();
	}
	std::cout << _data << " - " << _count << "\n";
	if (_right)
	{
		_right->printNodes();
	}
}

template <class data>
BSNode<data>* BSNode<data>::operator=(const BSNode<data>& other)
{
	return new BSNode<data>(other);
}

template <class data>
bool BSNode<data>::isLeaf() const
{
	if ((this) && (_left || _right))
	{
		return false;
	}
	else
	{
		return true;
	}
}

template <class data>
data BSNode<data>::getData() const
{
	return _data;
}

template <class data>
BSNode<data>* BSNode<data>::getLeft() const
{
	return _left;
}

template <class data>
BSNode<data>* BSNode<data>::getRight() const
{
	return _right;
}

template<class data>
inline void BSNode<data>::setLeft(BSNode<data>* left)
{
	_left = left;
}

template<class data>
inline void BSNode<data>::setRight(BSNode<data>* right)
{
	_right = right;
}

template <class data>
bool BSNode<data>::search(data val) const
{
	if (val == _data)
	{
		return true;
	}
	else if (this != nullptr || isLeaf())
	{
		return false;
	}

	if ((_left && _left->search(val)) || (_right && _right->search(val)))
	{
		return true;
	}
	else
	{
		return false;
	}
}

template <class data>
int BSNode<data>::getHeight() const
{
	int left, right;
	if (this && !isLeaf())
	{
		if ((_left && _right && _left->getHeight() > _right->getHeight()) || (_left && !_right))
		{
			return _left->getHeight() + 1;
		}
		else
		{
			return _right->getHeight() + 1;
		}
	}
	else
	{
		return 1;
	}
}

template <class data>
int BSNode<data>::getDepth(const BSNode<data>& root) const
{
	return root.getCurrNodeDistFromInputNode(this);
}

template<class data>
inline int BSNode<data>::getBalanceFactor()
{
	return 0;
}

template<class data>
inline BSNode<data>* BSNode<data>::rotateLeft()
{
	return NULL;
}

template<class data>
inline BSNode<data>* BSNode<data>::rotateRight()
{
	return NULL;
}

template <class data>
int BSNode<data>::getCurrNodeDistFromInputNode(const BSNode<data>* node) const
{
	int left, right;
	if (search(node->getData()) == 0)
	{
		return -1;
	}
	if (node->getData() == this->getData())
	{
		return 0;
	}
	else
	{
		_left ? left = _left->getCurrNodeDistFromInputNode(node) : left = 0;
		_right ? right = _right->getCurrNodeDistFromInputNode(node) : right = 0;
		if ((_left && _right && left > right) || (_left && !_right))
		{
			return left + 1;
		}
		else
		{
			return right + 1;
		}
	}
}
