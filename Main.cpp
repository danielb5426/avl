#include "BSNode.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>
#include "printTreeToFile.h"
#include "AVLTree.h"

using std::cout;
using std::endl;

int main()
{
	std::string stringArr[] = { "9", "8", "7", "6", "5", "9.5", "9.6", "9.9"};
	std::string string = "BSTData.txt";
	BSNode<std::string>* stringArray = new AVLTree<std::string>(stringArr[0]);

	for (int i = 1; i < 8; i++)
	{
		stringArray = stringArray->insert(stringArr[i]);
	}

	std::ofstream file(string);

	printTreeToFile(stringArray, file);
	file.close();
	system("BinaryTree.exe");
	system("pause");
	return 0;
}
