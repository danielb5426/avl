#pragma once
#include "BSNode.h"
#include <string>
#include <sstream>
#include <iostream>
#include <exception>

template<class data>
class AVLTree : public BSNode<data>
{
public:

	AVLTree(data data);
	~AVLTree();

	int getBalanceFactor();
	BSNode<data>* rotateLeft();
	BSNode<data>* rotateRight();
	BSNode<data>* insert(data value);

};

template<class data>
AVLTree<data>::AVLTree(data val):
	BSNode<data>(val)
{
}
template<class data>
AVLTree<data>::~AVLTree()
{
}

template<class data>
int AVLTree<data>::getBalanceFactor()
{
	return (int)this->_left->getHeight() - (int)this->_right->getHeight();
}

template<class data>
BSNode<data>* AVLTree<data>::rotateLeft()
{
	BSNode<data>* retu = this->_right;
	BSNode<data>* x = this->_right->getLeft();
	this->_right->setLeft(this);
	this->_right = x;

	return retu;
}

template<class data>
BSNode<data>* AVLTree<data>::rotateRight()
{
	BSNode<data>* retu = this->_left;
	BSNode<data>* x = this->_left->getRight();
	this->_left->setRight(this);
	this->_left = x;

	return retu;
}

template<class data>
BSNode<data>* AVLTree<data>::insert(data value)
{
	int balance;
	if (value > this->_data)
	{
		if (this->_right)
		{
			this->_right = this->_right->insert(value);
		}
		else
		{
			this->_right = new AVLTree<data>(value);
			this->_right->setLeft(nullptr);
			this->_right->setRight(nullptr);
		}
	}
	else if (value < this->_data)
	{
		if (this->_left)
		{
			this->_left = this->_left->insert(value);
		}
		else
		{
			this->_left = new AVLTree<data>(value);
			this->_left->setLeft(nullptr);
			this->_left->setRight(nullptr);
		}
	}
	else
	{
		throw ;
	}

	balance = getBalanceFactor();

	if (balance > 1 || balance < -1)
	{
		if (this->_left && balance > 0 && this->_left->getBalanceFactor() > 0)// LL
		{
			return rotateRight();
		}
		else if (this->_right && balance > 0 && this->_right->getBalanceFactor() < 0)// LR
		{

			this->_left = this->_left->rotateLeft();
			return rotateRight();
		}
		else if (this->_right && balance < 0 && this->_right->getBalanceFactor() < 0)// RR
		{
			return rotateLeft();
		}
		else if (this->_left && balance < 0 && this->_left->getBalanceFactor() > 0)// RL
		{
			this->_right = this->_right->rotateRight();
			return rotateLeft();
		}
	}
	return this;
}
